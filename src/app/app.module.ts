import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MeetingScreenComponent } from './component/meeting-screen/meeting-screen.component';
// For MDB Angular Free
import { NavbarModule, WavesModule, ButtonsModule } from 'angular-bootstrap-md';
import {MDBBootstrapModule } from 'angular-bootstrap-md';
import { ModalComponent } from '../app/component/modal/modal.component';
import { WindowsMeetingComponent } from './component/windows-meeting/windows-meeting.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SafePipe } from './safe.pipe';




@NgModule({
  declarations: [
    AppComponent,
    MeetingScreenComponent,
    ModalComponent,
    WindowsMeetingComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NavbarModule,
    WavesModule,
    ButtonsModule,
    MDBBootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
