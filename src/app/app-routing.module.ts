import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MeetingScreenComponent} from './component/meeting-screen/meeting-screen.component';
import {ModalComponent} from './component/modal/modal.component';


const routes: Routes = [
  { path: 'MeetingScreenComponent', component: MeetingScreenComponent },
  { path: 'ModalComponent', component: ModalComponent },
  { path: '**', redirectTo: 'MeetingScreenComponent' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
