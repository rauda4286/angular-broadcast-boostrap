import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WindowsMeetingComponent } from './windows-meeting.component';

describe('WindowsMeetingComponent', () => {
  let component: WindowsMeetingComponent;
  let fixture: ComponentFixture<WindowsMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WindowsMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindowsMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
