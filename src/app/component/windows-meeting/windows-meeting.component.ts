import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';



@Component({
  selector: 'app-windows-meeting',
  templateUrl: './windows-meeting.component.html',
  styleUrls: ['./windows-meeting.component.css'],
})
export class WindowsMeetingComponent implements OnInit {
  mData: any;
  @ViewChild(ModalComponent) modal: ModalComponent;
  constructor() {
    this.mData = { text: '', code: '', url: '' };
  }
  ngOnInit(): void {
  }
  modalOpen($event) {
    this.modal.openXl();
    console.log($event);
  }
  recieveData($event) {
    this.mData = { ...$event };
    console.log($event);
  }
}
