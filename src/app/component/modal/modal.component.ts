import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbAlertConfig} from '@ng-bootstrap/ng-bootstrap';


interface modalData {
  text: string;
  code: string;
  url: string;
}


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  providers: [NgbModalConfig, NgbModal, NgbAlertConfig],
})
export class ModalComponent implements OnInit {
  mData: modalData;
  @Output() textEmitter = new EventEmitter<modalData>();
  @ViewChild('content') content;
  @Input() public alerts: Array<string> = [];

  constructor(config: NgbModalConfig, public modalService: NgbModal, alertConfig: NgbAlertConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
    this.mData = { text: '', code: '', url: '' };
    alertConfig.type = 'success';
    alertConfig.dismissible = false;
  }
  openXl() {
    this.modalService.open(this.content, { size: 'xl' });
  }
  sendText() {
    this.textEmitter.emit(this.mData);
    this.alerts = [];
  }
  ngOnInit(): void {}
}
