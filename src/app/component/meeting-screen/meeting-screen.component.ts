import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meeting-screen',
  templateUrl: './meeting-screen.component.html',
  styleUrls: ['./meeting-screen.component.css'],
})
export class MeetingScreenComponent implements OnInit {
  show: boolean;
  hideMenu: string;
  showMenu: string;
  constructor() {}

  ngOnInit(): void {}
  toggle() {
    this.show = !this.show;
    if (this.show) {
      this.hideMenu = 'Hide';
    } else {
      this.showMenu = 'Show';
    }
  }
}
